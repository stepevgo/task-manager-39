package ru.t1.stepanishchev.tm.api.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUsername();

    @NotNull String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

}
